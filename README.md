# HuNI Harvest and Transformation Pipeline

## Harvest process

The top-level script is [harvest.py](./harvest-and-transformation-pipeline/src/master/bin/harvest.py) - it walks through all our partner sites and
updates every feed. This gets run every night.

Depending on the feed type the core harvest code is different:

* Simple - [simple.py](./harvest-and-transformation-pipeline/src/master/huni/harvest/simple.py) - proces
    * TODO
* OAI - [oai.py](./harvest-and-transformation-pipeline/src/master/huni/harvest/oai.py) - process
    - fetch the (partial) list of records using the ?verb=ListIdentifiers url
    - grab the resumptionToken if there is one (we need that later on)
    - work out how many records we are fetching (just so we can display it)
    - walk through the current list of records and download each one to a file
    - if we have a resumptionToken, continue the process with the next batch

The above code relies on the following helper - [base.py](./harvest-and-transformation-pipeline/src/master/huni/harvest/base.py) - the code that actually downloads the files is in there. It users the Python ['Requests' library](http://docs.python-requests.org/en/latest/)

## Overview of repository

* bin: pipeline tools
* configs: configuration files
* huni: the HuNI python module

## Dependencies

* [HuNI Python](http://wiki.huni.net.au/display/admins/HuNI+Python+Distribution)

## Installation

> bin/build-and-deploy

## Tests 

To run all of the tests (you're in the top level of the checkout):

> nosetests -v  -s --with-cov --cov-report term-missing

To run a set of tests in a specific file: 

> nosetests -v  -s --with-cov --cov-report term-missing test_preprocess.py 

## Running the transformer

Look at the examples in the test scripts. There are a bunch of helpers in this folder also.