#!/usr/bin/env python

# Author: Dr Marco La Rosa <marco@larosa.org.au>
#
# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without 
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice, 
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice, 
#    this list of conditions and the following disclaimer in the documentation 
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
#  POSSIBILITY OF SUCH DAMAGE.

from lxml import etree
import os
import os.path
import sys
import argparse
import logging 
import hashlib
from huni.counter import Counter

log = logging.getLogger('AFIRC')

class Flattenizer:
    def __init__(self):
        pass

    def flatten(self, monolithic_datafile, output):
        """Iterate over the monolithic datafile producing individual records"""

        for event, element in etree.iterparse(monolithic_datafile, 
            tag='{http://www.inmagic.com/webpublisher/query}Record'):

            total_records = element.getparent().get('setCount')
            break

        i = 0
        c = Counter(int(total_records))
        for event, element in etree.iterparse(monolithic_datafile,
            tag='{http://www.inmagic.com/webpublisher/query}Record'):

            i += 1
            c.update(i, log)

            entity_id = element.xpath('inm:ID', namespaces= { 'inm': 'http://www.inmagic.com/webpublisher/query' })[0].text
            e = element.xpath('inm:Record-Type', namespaces= { 'inm': 'http://www.inmagic.com/webpublisher/query' })[0].text
            try:
                entity_name = e.replace(' ', '_')
            except AttributeError:
                #print etree.tostring(element, pretty_print=True)
                log.error('Skipping entity ID: %s' % entity_id)
                log.error("\n%s" % etree.tostring(element, pretty_print=True))
                #print sys.exc_info()
                continue

            datafile = os.path.join(output, "%s-%s-%s.xml" % ('oai-afirc.rmit.edu.au', entity_name, entity_id))
            log.debug("Writing %s" % datafile) 
            self.write(element, datafile)

            element.clear()

    def write(self, document, datafile):
        """Write document to datafile"""

        f = open(datafile, 'w')
        f.write(etree.tostring(document, pretty_print=True))
        f.close()

    def get_hash(self, fname):
        """Given a filename, return the md5 hash"""
        with open(fname, 'r') as fh:
            try:
                m = hashlib.md5()
                data = fh.read()
                m.update(data)
                return m.hexdigest()
            except:
                log.error('Error in the hash function :: OHRM code')
                log.debug(sys.exc_info())

    def resources(self, output):
        "Update the resources file"""
        i = 0
        files = [name for name in os.listdir(output)]
        total = len(files)
        log.debug("Total number of files in set: %s" % total)
        #log.debug(files)

        root = etree.Element('resources', recordCount=str(total))
        i = 0
        c = Counter(total)
        for f in files:
            i += 1
            c.update(i, log)
            if f.endswith('.xml'):
                record = etree.SubElement(root, 'record')
                name = etree.SubElement(record, 'name')
                name.text = str(f)
                filehash = etree.SubElement(record, 'hash')
                filehash.text = self.get_hash(os.path.join(output, f))
                log.debug(etree.tostring(record, pretty_print=True))

        #log.debug(etree.tostring(root, pretty_print=True))

        f = open(os.path.join(output, 'resources.xml'), 'w')
        f.write(etree.tostring(root, pretty_print=True))
        f.close()

if __name__ == "__main__":

    # read and check the options
    parser = argparse.ArgumentParser(description='Flatten the AFIRC dataset')
    parser.add_argument('--datafile', required=True, dest='datafile',
        help="The path to the unzippped, monolithic datafile. We're assuming it's been retrieved and unpacked locally already.")
    parser.add_argument('--output', required=True, dest='output',
        help="Location to write the individual files to.")

    parser.add_argument('--debug', dest='debug', action='store_true', help='Turn on full debugging (includes --info)')
    parser.add_argument('--info', dest='info', action='store_true', help='Turn on informational messages')
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    if args.info:
        logging.basicConfig(level=logging.INFO)

    if not (args.debug and args.info):
        # just give us error messages
        logging.basicConfig(level=logging.ERROR)

    f = Flattenizer()
    f.flatten(args.datafile, args.output)
    f.resources(args.output)
