#!/usr/bin/env python

# Author: Dr Marco La Rosa <marco@larosa.org.au>
#
# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without 
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice, 
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice, 
#    this list of conditions and the following disclaimer in the documentation 
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
#  POSSIBILITY OF SUCH DAMAGE.

import sys
import os.path
import argparse
import logging
from huni.counter import Counter
from huni.analyse import Analyser

log = logging.getLogger('ANALYSER')

if __name__ == "__main__":

    # read and check the options
    parser = argparse.ArgumentParser(description='XML Analyser')

    parser.add_argument('--harvest', required=True, dest='harvest', 
        help='The path to the harvested data to be analysed. This will be something like /srv/aggregate/(DATE)/(SITE)/clean.\
        You need to analyse the clean directory as the analyser is expecting to count types from the commonly mapped\
        filenames')

#    parser.add_argument('--map-by-selector', action="store_true", dest='mapbyselector', 
#        help='Determine the entity type by an XPATH entity_type on the document.')
#    parser.add_argument('--namespace', dest='namespace', 
#        help='When using --map-by-selector, you need to provide the namespace for the XPATH selector. \
#        Something like: "doc:::urn:isbn:1-931666-33-4"; where the part before the first ":::" \
#        is the prefix and the rest is the actual namespace.')
#    parser.add_argument('--entity', dest='entity_type', 
#        help='When using --map-by-selector, you need to provide the XPATH entity_type for the entity type \
#        element inside the document. Something like: "/doc:eac-cpf/doc:cpfDescription/doc:identity/doc:entityType". \
#        Note the prefix "doc" we defined in the --namespace argument.')
#    parser.add_argument('--attribute', dest='attrib', 
 #       help="If the type is contained in an attribute, define that attribute here.")

    parser.add_argument('--nexamples', dest='nexamples', default='5', 
        help='How many example documents of each entity type to output = default 5.')

    parser.add_argument('--debug', dest='debug', action='store_true', help='Turn on full debugging (includes --info)')
    parser.add_argument('--info', dest='info', action='store_true', help='Turn on informational messages')

    args = parser.parse_args()

    # unless we specify otherwise
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    if args.info:
        logging.basicConfig(level=logging.INFO)

    if not (args.debug and args.info):
        # just give us error messages
        logging.basicConfig(level=logging.ERROR)

    a = Analyser(args.nexamples)
    a.process(args.harvest)
