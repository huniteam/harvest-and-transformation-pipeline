#!/usr/bin/env python

# Author: Dr Marco La Rosa <marco@larosa.org.au>
#
# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without 
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice, 
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice, 
#    this list of conditions and the following disclaimer in the documentation 
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
#  POSSIBILITY OF SUCH DAMAGE.

import sys
import os
import os.path
import shutil
import argparse
import logging


if __name__ == "__main__":

    # read and check the options
    parser = argparse.ArgumentParser(description='Clean harvest folder. Removes all folders except those \
    with data.')
    parser.add_argument('--harvest', dest='harvest', required=True,
        help='The path to the site harvest folder. This will be something like "/srv/aggregate/(DATE TODAY)/(SITE CODE)". \
        The tool expects to find a folder "original" as a child of that path.')

    parser.add_argument('--info', dest='info', action='store_true', help='Turn on informational messages')
    parser.add_argument('--debug', dest='debug', action='store_true', help='Turn on full debugging (includes --info)')

    args = parser.parse_args()

    # unless we specify otherwise
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    if args.info:
        logging.basicConfig(level=logging.INFO)

    if not (args.debug and args.info):
        # just give us error messages
        logging.basicConfig(level=logging.ERROR)

    # get the logger
    log = logging.getLogger('HARVEST_CLEANER')

    try:
        originals = os.path.join(args.harvest, 'original')
        try:
            for (dirpath, dirnames, filenames) in os.walk(originals):
                filenames = [ f for f in filenames if f != 'resources.xml' ]
                if len(filenames) == 0:
                    log.info("Removing empty folder: %s" % originals)
                    shutil.rmtree(originals)

                    log.info("Removing empty folder: %s" % os.path.dirname(originals))
                    os.rmdir(os.path.dirname(originals))

        except OSError:
            # HARVEST/SITE/original triggered an OSError so we
            #  fail out here.
            log.error("Not sure you're in the right place!")
            log.error("I was expecting to find an 'original' folder.")
            log.error("Here's what I can see: %s" % originals)
            sys.exit()

        feeds = os.listdir(args.harvest)
        if len(feeds) == 0:
            log.info("Removing empty folder: %s" % args.harvest)
            os.rmdir(args.harvest)
    except OSError:
        log.info("Nothing to do.")



