#!/usr/bin/env python

# Author: Dr Marco La Rosa <marco@larosa.org.au>
#
# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

import sys
import os
import os.path
import argparse
import logging
from lxml import etree
from datetime import datetime, date

def generate_rss2_feed(stats):

    rss = etree.Element('rss', version='2.0')
    channel = etree.SubElement(rss, "channel")

    title = etree.SubElement(channel, 'title')
    title.text = "HuNI Harvest Feed"

    link = etree.SubElement(channel, 'link')
    link.text = "http://data.huni.net.au/status"

    lang = etree.SubElement(channel, 'language')
    lang.text = "en-us"

    pubDate = etree.SubElement(channel, 'pubDate')
    pubDate.text = datetime.strftime(datetime.now(), "%c")
    lastBuildDate = etree.SubElement(channel, 'lastBuildDate')
    lastBuildDate.text = datetime.strftime(datetime.now(), "%c")

    for s in stats:
        today = datetime.today()
        if stats[s][4] == str(datetime.strftime(today, "%Y-%m-%d")):
            item = etree.SubElement(channel, 'item')
            title = etree.SubElement(item, 'title')
            title.text = s
            link = etree.SubElement(item, 'link')
            link.text = os.path.join('http://data.huni.net.au/aggregate', stats[s][4], s)
            guid = etree.SubElement(item, 'guid')
            guid.text = link.text
            description = etree.SubElement(item, 'description')
            description.text = "New data harvested from %s" % s
            pubDate = etree.SubElement(item, 'pubDate')
            pubDate.text = datetime.strftime(datetime.now(), "%c")

    f = open('/srv/feed/updates.xml', 'w')
    f.write(etree.tostring(rss, pretty_print=True))
    f.close()

    log.debug(etree.tostring(rss, pretty_print=True))

def generate_report(input_folder):
    stats = {}
    harvests = sorted(os.listdir(input_folder))
    harvests = [ d for d in harvests if not d.startswith('.') ]
    for date in harvests:
        log.info("Processing %s" % date)
        base = os.path.join(os.path.join(input_folder, date))
        sites = [ d for d in os.listdir(base) if not d.startswith('.') ]

        for site in sites:
            original_folder = os.path.join(base, site, 'original')
            noriginal = 0
            if os.path.exists(original_folder):
                (path, dirs, files) = os.walk(original_folder).next()
                noriginal = len(files)

            clean_folder = os.path.join(base, site, 'clean')
            nclean = 0
            if os.path.exists(clean_folder):
                (path, dirs, files) = os.walk(clean_folder).next()
                nclean = len(files)

            invalid_folder  = os.path.join(base, site, 'invalid')
            ninvalid = 0
            if os.path.exists(invalid_folder):
                (path, dirs, files) = os.walk(invalid_folder).next()
                ninvalid = len(files)

            solr_folder = os.path.join(base, site, 'solr')
            nsolr = 0
            if os.path.exists(solr_folder):
                (path, dirs, files) = os.walk(solr_folder).next()
                nsolr = len(files)

            stats[site] = [ str(noriginal), str(nclean), str(ninvalid), str(nsolr), str(date) ]

    status = os.path.join(input_folder, '.status.txt')
    f = open(status, 'w')
    for site in sorted(stats.keys()):
        data = "%s, %s\n" % (site, ', '.join(stats[site]))
        f.write(data)
    f.close()

    return stats

if __name__ == "__main__":
    # read and check the options
    parser = argparse.ArgumentParser(description='Generate the status report.')

    parser.add_argument('--input', dest='input', required=True, \
        help='The path to the root of the aggregate. It will likely be /srv/aggregate.')

    parser.add_argument('--debug', dest='debug', action='store_true', help='Turn on full debugging (includes --info)')
    parser.add_argument('--info', dest='info', action='store_true', help='Turn on informational messages')
    args = parser.parse_args()

    # unless we specify otherwise
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    if args.info:
        logging.basicConfig(level=logging.INFO)

    if not (args.debug and args.info):
        # just give us error messages
        logging.basicConfig(level=logging.ERROR)

    # get the logger
    log = logging.getLogger('STATUS')

    # generate the report
    stats = generate_report(args.input)

    # generate the rss 2 feed
    generate_rss2_feed(stats)

