#!/usr/bin/env python

# Author: Dr Marco La Rosa <marco@larosa.org.au>
#
# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without 
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice, 
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice, 
#    this list of conditions and the following disclaimer in the documentation 
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
#  POSSIBILITY OF SUCH DAMAGE.

from lxml import etree
import os
import os.path
import sys
import argparse
import ConfigParser
import logging
import hashlib
import requests
import subprocess
import copy
import re
from datetime import datetime, timedelta, date
from huni.harvest.harvest import Harvest

if __name__ == "__main__":

    # read and check the options
    parser = argparse.ArgumentParser(description='Harvest the dataset feeds')
    parser.add_argument('--config', required=True, dest='cfg',
        help="Configuration file.")
    parser.add_argument('--repository', required=True, dest='repository',
        help="The base harvest (or aggregate) folder. This will be the root of the datestamped harvest folders.")
    parser.add_argument('--site', dest='site', default=None, help='Sometimes you just want to harvest a single site')
    parser.add_argument('--resync', dest='resync', action='store_true', default=False, \
        help='Sometimes you want to reharvest from scratch.\
        This option tells the harvester to ignore diffs and last harvest time and start from scratch.')
    parser.add_argument('--previous', dest='previous', default=None, help="For simple sites it's sometimes necessary \
    to compare to a date earlier than the most recent harvest (ie to check the harvest). In this case, use this argument \
    specifying the date in the form YYYY-MM-DD.")

    parser.add_argument('--debug', dest='debug', action='store_true', help='Turn on full debugging (includes --info)')
    parser.add_argument('--info', dest='info', action='store_true', help='Turn on informational messages')
    args = parser.parse_args()

    # unless we specify otherwise
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    if args.info:
        logging.basicConfig(level=logging.INFO)

    if not (args.debug and args.info):
        # just give us error messages
        logging.basicConfig(level=logging.ERROR)

    h = Harvest(args.cfg, args.repository, args.resync)
    h.update(args.site, args.previous)

    # if we get to here **WITHOUT** an error then we must have succeeded doing
    #  something. So - let's say so and then the status report can check for it
    print "%s:::HARVEST:::ok" % args.site
