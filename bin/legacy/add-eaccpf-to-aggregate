#!/bin/bash

# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without 
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice, 
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice, 
#    this list of conditions and the following disclaimer in the documentation 
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
#  POSSIBILITY OF SUCH DAMAGE.

# source the configuration
. /etc/default/huni

[[ $# != 2 ]] && echo "Pass in a single EAC-CPF datasource to add to the aggregate and the date to process. Exiting now." && exit 0

[[ -z $SOLR_SERVER ]] && echo "Solr server undefined. Define it!" && exit 0

#echo "Updating $1"
HARVEST_FOLDER=$HARVEST/$2/$1/original
[[ -e $HARVEST/$2/$1/clean ]] && HARVEST_FOLDER=$HARVEST/$2/$1/clean
transform-to-solr.py  --site $1 --harvest $HARVEST_FOLDER --output $HARVEST/$2/$1/solr \
                --transforms $TRANSFORMS/$1 \
                --map-by-selector --namespace n:::urn:isbn:1-931666-33-4 \
                --entity //n:eac-cpf/n:cpfDescription/n:identity/n:entityType \
                --entity_id //n:eac-cpf/n:control/n:recordId \
                --info