#!/bin/bash

# Author: Dr Marco La Rosa <marco@larosa.org.au>
#
# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without 
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice, 
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice, 
#    this list of conditions and the following disclaimer in the documentation 
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
#  POSSIBILITY OF SUCH DAMAGE.

# source the global configuration
. /etc/default/huni

harvest() {
    # $1: site to harvest
    harvest.py --config /etc/huni/harvest/harvest.cfg --info --site $1
}

harvest_afirc() {
    mkdir -p $HARVEST/AFIRC
    wget http://afidb.adc.rmit.edu.au/dbtw-wpd/feed/afirc.xml.zip -O $HARVEST/AFIRC/afirc.xml.zip
    cd $HARVEST/AFIRC
    [[ -f afirc.xml ]] && rm afirc.xml
    unzip afirc.xml.zip
    rm afirc.xml.zip
    /usr/local/bin/afirc-huni-exporter.py --config /etc/huni/AFIRC/afirc.conf
}

## HARVEST and INGEST EAC-CPF resources
SITES="AWAP EOAS EMEL FCAC FCNA FCNT FCNS FCQD FCSA FCTS FCVC FCWA GOLD SAUL WALL ADB OA DAAO"
for site in $SITES ; do
    harvest $site
    /usr/local/bin/add-eaccpf-to-aggregate $site
    /usr/local/bin/archive $site
done

## HARVEST and INGEST home brew resources
SITES="Bonza CAARP CircusOz AusStage"
for site in $SITES ; do
    harvest $site
    /usr/local/bin/add-other-to-aggregate $site
    /usr/local/bin/archive $site
done

## HARVEST and INGEST Paradisec resources (RIF-CS)
SITES="ParadisecA ParadisecB"
for site in $SITES ; do
    harvest $site
    /usr/local/bin/add-paradisec-to-aggregate $site
    /usr/local/bin/archive $site
done

## HARVEST and INGEST AFIRC - special case monolithic data file
harvest_afirc
/usr/local/bin/add-other-to-aggregate AFIRC
/usr/local/bin/archive AFIRC

