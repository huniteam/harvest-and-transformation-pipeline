#!/usr/bin/env python

# Author: Dr Marco La Rosa <marco@larosa.org.au>
#
# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without 
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice, 
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice, 
#    this list of conditions and the following disclaimer in the documentation 
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
#  POSSIBILITY OF SUCH DAMAGE.

import sys
import os
import os.path
import argparse
import logging
from huni.preprocess.deleted import Deleted

# get the logger
log = logging.getLogger('PREPROCESS')

if __name__ == "__main__":
    # read and check the options
    parser = argparse.ArgumentParser(description='Pre-processor')

    parser.add_argument('--input', required=True, dest='input',
        help='The harvest folder to process. This will be something like /srv/harvest/(DATE)/(SITE CODE). \
        This tool then expects to find a folder "original" and it will create a folder "clean".')

    parser.add_argument('--type', required=True, dest='type', \
        choices=['AustLit', 'DAAO', 'EAC', 'Heurist', 'MARC21', 'Omeka', 'PDSC', 'Simple'], \
        help='The preprocessor to run: AustLit, DAAO, EAC, Heurist, MARC21, Omeka, PDSC, or Simple')

    parser.add_argument('--info', dest='info', action='store_true', help='Turn on informational messages')
    parser.add_argument('--debug', dest='debug', action='store_true', help='Turn on full debugging (includes --info)')

    args = parser.parse_args()

    # unless we specify otherwise
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    if args.info:
        logging.basicConfig(level=logging.INFO)

    if not (args.debug and args.info):
        # just give us error messages
        logging.basicConfig(level=logging.ERROR)

    if args.type == 'EAC':
        from huni.preprocess.eac import Preprocess
    elif args.type == 'PDSC':
        from huni.preprocess.paradisec import Preprocess
    elif args.type == 'AustLit':
        from huni.preprocess.austlit import Preprocess
    elif args.type == 'Heurist':
        from huni.preprocess.heurist import Preprocess
    elif args.type == 'MARC21':
        from huni.preprocess.marc21 import Preprocess
    elif args.type == 'DAAO':
        from huni.preprocess.daao import Preprocess
    elif args.type == 'Omeka':
        from huni.preprocess.omeka import Preprocess
    else:
        from huni.preprocess.simple import Preprocess

    input_folder = os.path.join(args.input, "original")
    output_folder = os.path.join(args.input, "clean")
    if not os.path.exists(input_folder):
        log.error("Can't find %s. Did you specify the input as /srv/aggregate/(DATE)/(SITE CODE)?" % input_folder)
        sys.exit()

    # ensure the clean folder exists
    if not os.path.exists(output_folder) and not os.path.isdir(output_folder):
        os.makedirs(output_folder)

    (base, site) = os.path.split(args.input)
    (base, date) = os.path.split(base)
    # base = /srv/aggregate

    # see if there are deleted records; if so, then for each
    #  - find a predecessor - we need the type and id from it
    #  - create a clean version named as the type and id but with *data removed* as content
    deleted_files = os.path.join(input_folder, 'deleted_records.xml')
    if os.path.isfile(deleted_files):
        d = Deleted(deleted_files)
        d.doit(typeof=args.type)

    # now preprocess the good data
    p = Preprocess(input_folder, output_folder, site)
    p.doit()

    # if we get to here **WITHOUT** an error then we must have succeeded doing
    #  something. So - let's say so and then the status report can check for it
    print "%s:::PREPROCESS:::ok" % site
