#!/usr/bin/env python

# Author: Dr Marco La Rosa <marco@larosa.org.au>
#
# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without 
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice, 
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice, 
#    this list of conditions and the following disclaimer in the documentation 
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
#  POSSIBILITY OF SUCH DAMAGE.

import sys
import os.path
import argparse
import logging
from lxml import etree
import requests
import json
from datetime import datetime
from huni.index import Index
from huni.counter import Counter

class Process:
    def __init__(self, solr, input_folder, date, local_root, web_root, no_commit):
        self.idx = Index(solr, None)

        self.date = datetime.strptime(date, '%Y-%m-%d')

        self.input_folder = input_folder
        self.local_root = local_root
        self.web_root = web_root
        self.no_commit = no_commit
        
    def process_path(self):
        for (dirpath, dirnames, filenames) in os.walk(self.input_folder):
            total = len(filenames)
            c = Counter(total)
            i = 0
            for f in filenames:
                i += 1
                c.update(i, log)
                self.process_document(os.path.join(dirpath, f))
        self.flush_index()

    def process_document(self, document):
        log.debug("Processing: %s" % document)
        document = os.path.join(self.input_folder, document)
        document_url = document.replace(self.local_root, self.web_root)

        # read in the document
        doc = etree.parse(document)

        # does it look like this document has already had it's history sorted out?
        history = doc.xpath("//*[local-name()='field' and @*[local-name()='name']='document_history']")
        if history:
            for elem in history:
                # we need to wipe the history in the file to support the roll back feature
                # consider: we get the file, it has some history, but we need to rebuild that history
                #  based on what's in solr - so, we wipe it
                elem.getparent().remove(elem)

        # get the document unique identifier
        docid = "//*[local-name()='field' and @*[local-name()='name']='docid']"
        docid = doc.xpath(docid)[0].text

        # and see if solr has a document with that id already
        query = "%s/select?q=docid:\"%s\"&fl=document_history&wt=json" % (args.solr, docid)
        response = requests.get(query)
        if response.status_code != 200:
            log.error("oops - something went wrong: %s" % response.status_code)
            return

        # the response we get back from solr
        data = json.loads(response.text)

        # extract the history - if any
        #  but remove any history later than the date we're processing : this 
        #  effectively rolls the aggregate back in time
        document_history = {}
        try:
            history = data['response']['docs'][0]['document_history']
            for hist in history:
                date = hist.lstrip(self.web_root).split('/')[0]
                if self.date > datetime.strptime(date, '%Y-%m-%d'):
                    document_history[hist] = True

        except IndexError:
            # no history available
            pass

        document_history = [ x for x in document_history.keys() ]
        self.update_history(doc, document_url, document_history)
        self.add_source_documents(doc, document_url)
        log.debug(etree.tostring(doc, pretty_print=True))

        self.update_index(doc, document)
        self.save_document(doc, document)

    def add_source_documents(self, doc, document_url):
        tmp = doc.xpath("//*[local-name()='doc']")[0]
        provider_source = doc.xpath("//*[local-name()='field' and @*[local-name()='name']='provider_source']")
        if len(provider_source) == 0:
            elem = etree.Element('field', name='provider_source')
            elem.text = document_url.replace('solr', 'clean')
            tmp.append(elem)

        # see if rdf source is in it - exit if it is
        #rdf_source = doc.xpath("//*[local-name()='field' and @*[local-name()='name']='rdf_source']")

    def update_history(self, doc, document_url, document_history):
        # then add the old history to the new and update the document
        if document_url not in document_history:
            document_history = document_history + [ document_url ]
        else:
            document_history = [ document_url ]

        log.debug("Adding the following history: %s" % document_history)
        tmp = doc.xpath("//*[local-name()='doc']")[0]
        for h in document_history:
            history = etree.Element('field', name="document_history")
            history.text = h
            tmp.append(history)

    def update_index(self, doc, document):
        self.idx.submit(etree.tostring(doc), document) 

    def save_document(self, doc, document):
        f = open(document, 'w')
        f.write(etree.tostring(doc))
        f.close()

    def flush_index(self):
        if not self.no_commit:
            self.idx.commit()
            self.idx.optimize()



if __name__ == "__main__":
    # read and check the options
    parser = argparse.ArgumentParser(description='Given a folder of solr documents, update the Index.')

    parser.add_argument('--input', dest='input', required = True,
        help='The path to the harvested site folder of the harvest to be processed. It will be something like \
        /srv/aggregate/(HARVEST DATE)/(SITE CODE). The tool expects to find a folder "solr" as a child of that path.')

    parser.add_argument('--solr', dest='solr', required = True,
        help='The url of the solr core.')
    parser.add_argument('--document', dest='document', help='A single document to process - useful for testing. \
            Just name the document here - e.g. PDSC_item_AC1-032.xml; the tool will find it relative to --input.')

    parser.add_argument('--local-root', dest='local_root', required = True,
        help='The root path of the local aggregate. Everything up to the date. eg. /srv/aggregate')
    parser.add_argument('--web-root', dest='web_root', required = True,
        help='The root url of the web-based aggregate. Everything up to the date. eg. http://data.huni.net.au/aggregate')

    parser.add_argument('--no-commit', dest='no_commit', action='store_true', help="Don't commit results to solr (for bulk updates)")

    parser.add_argument('--info', dest='info', action='store_true', help='Turn on informational messages')
    parser.add_argument('--debug', dest='debug', action='store_true', help='Turn on full debugging (includes --info)')

    args = parser.parse_args()

    # unless we specify otherwise
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    if args.info:
        logging.basicConfig(level=logging.INFO)

    if not (args.debug and args.info):
        # just give us error messages
        logging.basicConfig(level=logging.ERROR)

    # get the logger
    log = logging.getLogger('INDEXER')

    if not args.debug:
        if not args.solr:
            log.error("You haven't specified a solr server and you're not in debug mode.")
            sys.exit()

    input_folder = os.path.join(args.input, "solr")
    if not os.path.exists(input_folder):
        log.error("Can't find %s. Did you specify the input as /srv/aggregate/(DATE)/(SITE CODE)?" % input_folder)
        sys.exit()

    (base, site) = os.path.split(args.input)
    (base, date) = os.path.split(base)
    p = Process(args.solr, input_folder, date, args.local_root, args.web_root, args.no_commit)
    if args.document:
        p.process_document(args.document)
        p.flush_index()
    else:
        p.process_path()

    # if we get to here **WITHOUT** an error then we must have succeeded doing
    #  something. So - let's say so and then the status report can check for it
    print "%s:::PROCESS:::ok" % site


                
