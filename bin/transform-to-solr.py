#!/usr/bin/env python

# Author: Dr Marco La Rosa <marco@larosa.org.au>
#
# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without 
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice, 
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice, 
#    this list of conditions and the following disclaimer in the documentation 
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
#  POSSIBILITY OF SUCH DAMAGE.

import sys
import os.path
import argparse
import logging
from huni.transform.tosolr import Transform


if __name__ == "__main__":

    # read and check the options
    parser = argparse.ArgumentParser(description='Transform to Solr common schema.')

    parser.add_argument('--input', dest='input', required=True,
        help='The path to the harvested data to be transformed and mapped in to the Solr common schema. \
        This will be something like /srv/harvest/(DATE)/(SITE CODE). This tool then expects to find a folder \
        "clean" and it will create a folder "solr".')
    parser.add_argument('--document', dest='document', help='A single document to process - useful for testing. \
        Just name the document here - e.g. PDSC_item_AC1-032.xml; the tool will find it relative to --input.')

    parser.add_argument('--transforms', required=True, dest='transforms', 
        help='The path to the transforms to be used with this data.')

    parser.add_argument('--info', dest='info', action='store_true', help='Turn on informational messages')
    parser.add_argument('--debug', dest='debug', action='store_true', help='Turn on full debugging (includes --info)')

    args = parser.parse_args()

    # unless we specify otherwise
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    if args.info:
        logging.basicConfig(level=logging.INFO)

    if not (args.debug and args.info):
        # just give us error messages
        logging.basicConfig(level=logging.ERROR)

    # get the logger
    log = logging.getLogger('TRANSFORMER')

    input_folder = os.path.join(args.input, "clean")
    output_folder = os.path.join(args.input, "solr")
    if not os.path.exists(input_folder):
        log.error("Can't find %s. Did you specify the input as /srv/aggregate/(DATE)/(SITE CODE)?" % input_folder)
        sys.exit()

    (base, site) = os.path.split(args.input)
    t = Transform(site, args.transforms, input_folder, output_folder, debug=args.debug)

    # HOORAY!! do it; finally.
    if args.document:
        t.process_document(args.document)
    else:
        t.process_path()

    # if we get to here **WITHOUT** an error then we must have succeeded doing
    #  something. So - let's say so and then the status report can check for it
    print "%s:::TRANSFORM:::ok" % site

