# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

from lxml import etree
import sys
import os.path
import argparse
import logging
from huni.counter import Counter

# get the logger
log = logging.getLogger('ANALYSER')

class Analyser:
    def __init__(self, nexamples):
        self.etypes = {}
        self.nexamples = int(nexamples)

    def process(self, path):
        """Iterate over a directory of harvested datafiles

        @params:
        path: the directory of content to be analysed
        """
        for (dirpath, dirnames, filenames) in os.walk(path):
            total = len(filenames)

            i = 0
            c = Counter(total)
            for fname in filenames:
                self.process_document("%s/%s" % (dirpath, fname))
                i += 1
                c.update(i, log)

        print "Found the following entity types in this feed"
        for key, value in self.etypes.items():
            print "%s: %s records" % (key, len(value))
            for i in range(self.nexamples):
                try:
                    print "\t%s" % (value[i])
                except IndexError:
                    continue


    def process_document(self, path):
        """Process a single datafile

        @params:
        path: path to the file to be processed
        """
        filename = path.split('/')[-1]
        try:
            entity_type = filename.split(':::')[1]  # source:::type:::id
        except IndexError:
            log.error("I can't seem to get the entity type from %s, skipping it for now" % path)
            return None

        try:
            document_list = self.etypes[entity_type]
        except KeyError:
            document_list = []
            pass
        document_list.append(path)
        self.etypes[entity_type] = document_list
