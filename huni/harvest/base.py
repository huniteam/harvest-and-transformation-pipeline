# Author: Dr Marco La Rosa <marco@larosa.org.au>
#
# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without 
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice, 
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice, 
#    this list of conditions and the following disclaimer in the documentation 
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
#  POSSIBILITY OF SUCH DAMAGE.

from lxml import etree
import os
import os.path
import sys
import argparse
import ConfigParser
import logging
import hashlib
import requests
import subprocess
import copy
import re
import time
from datetime import datetime, timedelta, date

from huni.counter import Counter

# get the logger
log = logging.getLogger('HARVESTER')

# quieten the requests library
requests_log = logging.getLogger("requests")
requests_log.setLevel(logging.ERROR)

class Base:
    def __init__(self):
        pass

    def get_update_file(self, url, update_file):
        ## get the updated resources file
        try:
            resp = requests.get(url)

            if resp.status_code == 200:
                fh = open(self.update_file, 'w')
                fh.write(resp.content)
                fh.close()
            else:
                log.error("Couldn't get the file list for %s - 404 not found. Skipping site." % site)
                return
        except requests.exceptions.ConnectionError:
            log.error("Couldn't get the resources file for %s. Skipping site." % url)
            return

    def retrieve(self, site, resource, local_file):
        """Retrieve a web resource and save it as a local file

        @params:
        resource: a URL to a file for download
        local_file: the full qualified path of the local file
            to save the data to
        """
        log.debug("Downloading %s" % resource)
        try:
            resp = requests.get(resource) 
            if resp.status_code == 200:
                try:
                    fh = open(local_file, 'w')
                    fh.write(resp.content)
                    fh.close()
                except UnicodeEncodeError:
                    log.error("Broken resource %s" % resource)
                    log.error(resp.content)

            elif resp.status_code == 404:
                log.error("Couldn't retrieve file %s" % resource)

        except requests.exceptions.ConnectionError:
            log.error("Connection to %s broken." % site)
            #log.error(sys.exc_info())
            time.sleep(2)
            #sys.exit()
            




