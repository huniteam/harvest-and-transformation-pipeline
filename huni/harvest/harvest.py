# Author: Dr Marco La Rosa <marco@larosa.org.au>
#
# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without 
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice, 
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice, 
#    this list of conditions and the following disclaimer in the documentation 
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
#  POSSIBILITY OF SUCH DAMAGE.

from lxml import etree
import os
import os.path
import sys
import argparse
import ConfigParser
import logging
import hashlib
import requests
import subprocess
import copy
import re
import time
import glob
from datetime import datetime, timedelta, date
from huni.harvest.oai import OAI
from huni.harvest.simple import Simple
from huni.counter import Counter

# get the logger
log = logging.getLogger('HARVESTER')

# quieten the requests library
requests_log = logging.getLogger("requests")
requests_log.setLevel(logging.ERROR)

class Harvest:
    def __init__(self, cfg, repository, resync):
        # store the repo as an object variable
        self.repository = repository

        # and ensure it exists
        if not os.path.exists(self.repository):
            os.makedirs(self.repository)

        # also create a hidden folder for the resources files
        if not os.path.exists(os.path.join(self.repository, '.resources', str(date.today()))):
            os.makedirs(os.path.join(self.repository, '.resources', str(date.today())))

        # whether or not to perform a full site resync
        self.resync = resync

        # read in the config file
        self._ingest(cfg)

        # place to store deleted records
        self.deleted_records = []

    def _ingest(self, cfg):
        """Read the config file - set var's as instance var's"""
        config = ConfigParser.SafeConfigParser()
        config_file = config.read(cfg)
        if not config_file:
            log.error("Does that config file exist? %s" % cfg)
            sys.exit()

        self.datasets = {}
        for param, value in config.items('Datasets'):
            try:
                if len(value.split(',')) == 4:
                    (code, resource_type, frequency, resource) = value.split(',')
                    output_folder = None
                elif len(value.split(',')) == 5:
                    (code, resource_type, frequency, resource, output_folder) = value.split(',')
                    output_folder = output_folder.strip()
                else:
                    raise ValueError

                self.datasets[code.strip()] = { 
                    'resource': resource.strip(), 
                    'type': resource_type.strip(), 
                    'frequency': frequency.strip(),
                    'output_folder': output_folder
                }
            except ValueError:
                log.error("Looks like a datasource is incorrect. You provided '%s'" % value)
                log.error("Where it should be like 'Set2: EOAS, simple, weekly, http://www.findandconnect.gov.au/act/eac/resources.xml'")
                sys.exit()

    def update(self, process_only, previous=None):
        """Update the local harvest

        @params:
        process_only: if not None, harvest only this site

        Given the datasets in the config file, iterate over them and
        update the local copies of each as required.
        """
        # create today's harvest structure
        harvest_folder = os.path.join(self.repository, str(date.today()))
        log.debug("Today's harvest folder: %s" % harvest_folder)

        for site, provider in self.datasets.items():
            if process_only is not None and process_only != site:
                log.debug("Skipping %s; looking for %s" % (site, process_only))
                continue

            # today's data download
            #  some sites should be harvested in to a single folder: e.g. ParadisecA and B into PDSC
            if self.datasets[site]['output_folder'] is not None:
                site_repo = os.path.join(harvest_folder, self.datasets[site]['output_folder'], 'original')
            else:
                site_repo = os.path.join(harvest_folder, site, 'original')

            if not os.path.exists(site_repo):
                os.makedirs(site_repo)

            # is it an OAI endpoint or one of the more web2 ones...
            resource = provider['resource'] 
            if provider['type'] == 'OAI':
                # the resources file
                today = str(date.today())
                current_update_file = os.path.join(self.repository, '.resources', today, "%s%s" % (site, '-resources.xml'))
                log.debug("Resource file: %s" % provider['resource'])
                log.debug("Local resources file: %s" % current_update_file)

                oai = OAI(site, site_repo, provider, current_update_file, self.resync)
                oai.process_oai()
                deleted_records = oai.deleted_records

            elif provider['type'] == 'simple':
                # the resources file
                #  we're going to datestamp those too in case we need to diff harvest sets
                today = str(date.today())
                yesterday = str(date.today() - timedelta(days=1))
                current_update_file = os.path.join(self.repository, '.resources', today, "%s%s" % (site, '-resources.xml'))

                if previous is not None:
                    previous_update_file = os.path.join(self.repository, '.resources', previous, "%s%s" % (site, '-resources.xml')) 
                    if not os.path.exists(previous_update_file):
                        log.error("Couldn't find %s. Look in /srv/aggregate/.resources for available dates." % previous_update_file)
                        sys.exit()

                # in the sorted list of matches, we always want the last one 
                previous_update_file = [ f for f in sorted(glob.glob(os.path.join(self.repository, '.resources', '*', "%s%s" % (site, '-resources.xml')))) ]
                try:
                    previous_update_file = previous_update_file[-1:][0]
                except IndexError:
                    # no other update file
                    previous_update_file = None

               
                #previous_update_file = os.path.join(self.repository, '.resources', yesterday, "%s%s" % (site, '-resources.xml'))
                log.debug("Resource file: %s" % provider['resource'])
                log.debug("Current resources file: %s" % current_update_file)
                log.debug("Previous resources file: %s" % previous_update_file)

                simple = Simple(site, site_repo, provider, current_update_file, previous_update_file, self.resync)
                simple.process_simple()
                deleted_records = simple.deleted_records
                #self.process_simple(site, resource, site_repo, update_file)

            else:
                log.error("Unknown type '{0}' specified for {1}. Your options are: 'simple' or 'OAI'.".format(resource['type'], site))

            # write out the trace of deleted files
            if len(deleted_records) > 0:
                self.deleted_records_trace(site_repo)

    def deleted_records_trace(self, site_repo):
        root = etree.Element('deleted', recordCount=str(len(self.deleted_records)))
        for r in self.deleted_records:
            record = etree.SubElement(root, 'record')
            record.text = r.replace(':', '-').replace('/', '_')
        log.debug(etree.tostring(root, pretty_print=True))
        
        f = open(os.path.join(site_repo, 'deleted_records.xml'), 'w')
        f.write(etree.tostring(root, pretty_print=True))
        f.close()






