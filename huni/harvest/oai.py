# Author: Dr Marco La Rosa <marco@larosa.org.au>
#
# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

from lxml import etree
import os
import os.path
import sys
import argparse
import ConfigParser
import logging
import hashlib
import requests
import subprocess
import copy
import re
import time
from datetime import datetime, timedelta, date

from huni.counter import Counter
from huni.harvest.base import Base

# get the logger
log = logging.getLogger('HARVESTER')

# quieten the requests library
requests_log = logging.getLogger("requests")
requests_log.setLevel(logging.ERROR)

class OAI(Base):
    def __init__(self, site, repo, provider, update_file, resync):
        self.deleted_records = []

        self.site = site
        self.repo = repo
        self.update_file = update_file
        log.debug("Provider information: %s" % provider)

        # the URL to the data providers OAI service
        self.resource = provider['resource']

        if resync:
            self.tfrom = ''
        else:
            if provider['frequency'] == 'daily':
                # date now - one day to produce: &from=2013-06-13T07:56:44z
                seconds = 86400
            elif provider['frequency'] == 'weekly':
                # date now - one week to produce: &from=2013-06-13T07:56:44z
                seconds = 604800
            else:
                log.error("You specified a frequency of %s but I don't know what to do with that" % provider['frequency'])
                log.error("Re-read the docs in the config file and fix it. Setting to one day anyway.")
                seconds = 86400

            tfrom = datetime.now() - timedelta(0, seconds)
            self.tfrom = '&from=%s' % tfrom.strftime("%Y-%m-%d")

    def process_oai(self, token=None):
        log.info("Looking for updated resources: %s " % self.resource)
        if token is not None:
            get_url = re.sub('\?verb=.*', "?verb=ListIdentifiers&resumptionToken=" + token, self.resource)
            #resource = resource.replace('?verb=.*', "?verb=ListIdentifiers&resumptionToken=%s" % token)
            token = None
        else:
            get_url = self.resource + self.tfrom

        log.info("Updating the local data for %s" % self.site)
        log.info("Getting: %s" % get_url)
        self.get_update_file(get_url, self.update_file)

        ## first we need to get the resumptionToken - if it exists
        #   http://www.openarchives.org/OAI/2.0/guidelines-harvester.htm#resumptionToken
        for event, element in etree.iterparse(copy.deepcopy(self.update_file), tag='{http://www.openarchives.org/OAI/2.0/}resumptionToken'):
            token = element.text
            element.clear()

        ## then we need a count of total records
        total = 0
        for event, element in etree.iterparse(copy.deepcopy(self.update_file), tag='{http://www.openarchives.org/OAI/2.0/}header'):
            total += 1
            element.clear()
        log.info("Total records in set: %s" % total)

        i = 0
        c = Counter(total)
        ## then, for every 'record' in our current resources file
        for event, element in etree.iterparse(self.update_file, tag='{http://www.openarchives.org/OAI/2.0/}header'):
            try:
                if element.attrib['status'] == 'deleted':
                    self.deleted_records.append(element[0].text)
            except KeyError:
                pass

            i += 1

            c.update(i, log)

            # figure out the entity_name and datestamp
            entity_name = element[0].text
            entity_datestamp = element[0].getnext().text

            #  download the new file
            #site_repo = os.path.join(self.repository, site)
            provider_datafile = self.resource.replace('ListIdentifiers', 'GetRecord') + "&identifier=%s" % entity_name

            # and clean out the nasties in the name that will screw up our filesystem
            entity_name = entity_name.replace(':', '-').replace('/', '_')
            entity_local_datafile = os.path.join(self.repo, entity_name)
            log.debug("Local file: %s" % entity_local_datafile)
            log.debug("Provider datafile %s" % provider_datafile)

            # the MAGIC: get the file
            self.retrieve(self.site, provider_datafile, entity_local_datafile)

            # ditch the element so we save memory - we no longer need it
            element.clear()

        # we found a resumption token so back to the top and do it all again
        if token is not None:
            self.process_oai(token=token)


