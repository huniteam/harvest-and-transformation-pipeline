# Author: Dr Marco La Rosa <marco@larosa.org.au>
#
# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without 
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice, 
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice, 
#    this list of conditions and the following disclaimer in the documentation 
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
#  POSSIBILITY OF SUCH DAMAGE.

from lxml import etree
import os
import os.path
import sys
import argparse
import ConfigParser
import logging
import hashlib
import requests
import subprocess
import copy
import re
import time
from datetime import datetime, timedelta, date

from huni.counter import Counter
from huni.harvest.base import Base

# get the logger
log = logging.getLogger('HARVESTER')

# quieten the requests library
requests_log = logging.getLogger("requests")
requests_log.setLevel(logging.ERROR)

class Simple(Base):
    def __init__(self, site, repo, provider, current_update_file, previous_update_file, resync):
        self.deleted_records = []

        self.site = site
        self.repo = repo
        self.current_update_file = current_update_file
        self.previous_update_file = previous_update_file
        self.resync = resync
        log.debug("Provider information: %s" % provider)

        # the URL to the data providers OAI service
        self.resource = provider['resource']

    def get_content_list(self):
        ## get the updated resources file
        try:
            resp = requests.get(self.resource)
                 
            if resp.status_code == 200:
                # if the resources file is gzipped - unzip it and store it unzipped
                if self.resource.endswith('.gz'):
                    # get it, then save it to tmp file so gzip can deal with it
                    zipped_file = self.current_update_file + ".gz"
                    fh = open(zipped_file, 'wb')
                    fh.write(resp.content)
                    fh.close()
                    cmd = [ "/bin/gunzip", "--force", zipped_file ]
                    try:
                        returncode = subprocess.check_call(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    except subprocess.CalledProcessError:
                        log.error("Not sure if the resources file from %s is in order. Skipping site." % provider_base)
                        return
                else:
                    fh = open(self.current_update_file, 'w')
                    fh.write(resp.content)
                    fh.close()

            else:
                log.error("Couldn't get resources.xml file for %s - 404 not found. Skipping site." % self.site)
                return
        except requests.exceptions.ConnectionError:
            log.error("Couldn't get the resources file for %s. Skipping site." % self.site)
            return

    def process_simple(self):
        # get the data file then pass it to etree to walk
        provider_base = os.path.split(self.resource)[0]
        log.debug("Site base: %s" % provider_base)
        log.info("Updating the local data for %s, %s" % (self.site, self.resource)) 

        ## create the hash of existing files
        #log.info("Constructing the list of files in the local %s repo." % self.site)
        old_set = {}
        try:
            for event, element in etree.iterparse(self.previous_update_file, tag='record'):
                # figure out the entity_name and hash
                entity_name = element.xpath('name')[0].text
                entity_hash = element.xpath('hash')[0].text
                old_set[entity_name] = entity_hash
        except:
            pass

        # get the updated file list
        self.get_content_list()

        ## first we need a count of total records
        total = 0
        for event, element in etree.iterparse(copy.deepcopy(self.current_update_file), tag='record'):
            total += 1
            element.clear()
        log.info("Total records in set: %s" % total)

        i = 0
        c = Counter(total)
        ## then, for every 'record' in our current resources file
        for event, element in etree.iterparse(self.current_update_file, tag='record'):
            i += 1
            c.update(i, log)

            # figure out the entity_name and hash
            entity_name = element.xpath('name')[0].text
            entity_hash = element.xpath('hash')[0].text

            # construct the name of the resource to download and the local data filename
            entity_local_datafile = os.path.join(self.repo, entity_name)
            provider_datafile = os.path.join(provider_base, entity_name)
            log.debug("Entity datafile: %s" % entity_local_datafile)
            log.debug("Provider datafile: %s" % provider_datafile)

            # do a full resync
            if self.resync:
                self.retrieve(self.site, provider_datafile, entity_local_datafile)

            # compare the old hash to the new one - download if different
            elif old_set.has_key(entity_name):
                if entity_hash != old_set[entity_name]:
                    self.retrieve(self.site, provider_datafile, entity_local_datafile)
                    del old_set[entity_name]

            # never seen a local
            else:
                self.retrieve(self.site, provider_datafile, entity_local_datafile)

            # ditch the element so we save memory - we no longer need it
            element.clear()

        # whatever is left in the old_set has been deleted
        self.deleted_files = old_set
