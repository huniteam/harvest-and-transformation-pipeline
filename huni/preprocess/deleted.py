# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without 
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice, 
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice, 
#    this list of conditions and the following disclaimer in the documentation 
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
#  POSSIBILITY OF SUCH DAMAGE.

from lxml import etree
import sys
import os.path
import logging
import shutil
import glob
from huni.counter import Counter

# get the logger
log = logging.getLogger('DELETED_DATA')

class Deleted:
    def __init__(self, deleted_files):
        self.deleted_files = deleted_files
        (base, filename) = os.path.split(deleted_files)
        (base, filetype) = os.path.split(base)
        (base, site) = os.path.split(base)
        (base, date) = os.path.split(base)

        self.site = site
        self.date = date
        self.base = base

    def doit(self, typeof):
        if typeof == 'EAC':
            from huni.preprocess.eac import Preprocess
        elif typeof == 'PDSC':
            from huni.preprocess.paradisec import Preprocess
        else:
            from huni.preprocess.simple import Preprocess

        for event, elem in etree.iterparse(self.deleted_files, tag='record'):
            predecessors = [ f for f in sorted(glob.glob(os.path.join(self.base, '*', self.site, 'original', elem.text))) ]
            if predecessors:
                folderin = os.path.join(self.base, self.date, self.site, 'original')
                folderout  = os.path.join(self.base, self.date, self.site, 'clean')
                p = Preprocess(folderin, folderout, self.site)
                (identifier, doc) = p.get_identifier(predecessors[-1:][0])

                document_name = "%s.xml" % identifier
                doc = etree.Element('doc')
                deleted = etree.SubElement(doc,'deleted')
               
                filename = os.path.join(self.base, self.date, self.site, 'clean', document_name)
                f = open(filename, 'w')
                f.write(etree.tostring(doc, pretty_print=True))
                f.close()


