# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

from lxml import etree
import sys
import os.path
import logging
import shutil
from huni.counter import Counter

# get the logger
log = logging.getLogger('PREPROCESS_OMEKA')

omeka_ns = {
    'oai': 'http://www.openarchives.org/OAI/2.0/',
    'omeka': 'http://omeka.org/schemas/omeka-xml/v5'
}

itemtype_table = {
    '20': "organisation",
    '23': "work",
}

class Preprocess:
    def __init__(self, folderin, folderout, site):
        self.folder_in = folderin
        self.folder_out = folderout
        self.site = site

        if folderout is not None:
            self.folder_invalid = folderout.replace('clean', 'invalid')

            # ensure the clean folder exists
            if not os.path.exists(self.folder_out) and not os.path.isdir(self.folder_out):
                os.makedirs(self.folder_out)

        log.debug("Input folder: %s" % self.folder_in)
        log.debug("Output folder: %s" % self.folder_out)

    def isValid(self, document):
        # read the document to be transformed
        try:
            doc = etree.parse(document)
            return doc
        except:
            # handle invalid XML files that can't be read
            if not os.path.exists(self.folder_invalid) and not os.path.isdir(self.folder_invalid):
                os.makedirs(self.folder_invalid)

            shutil.copy(document, self.folder_invalid)
            return None

    def get_identifier(self, document):
        doc = self.isValid(document)
        if doc:
            try:
                items = doc.xpath('//oai:metadata/omeka:item', namespaces=omeka_ns)
                item_id = items[0].attrib['itemId']
                itemtypes = doc.xpath('//oai:metadata/omeka:item/omeka:itemType', namespaces=omeka_ns)
                itemtype_id = itemtypes[0].attrib['itemTypeId'] if len(itemtypes) > 0 else "000"
            except:
                log.error("Couldn't handle: %s" % document)
                return None, None
        else:
            return None, None

        etype = itemtype_table.get(itemtype_id, 'non-huni')
        identifier = "%s:::%s:::%s" % (self.site, etype, item_id)

        return identifier, doc

    def doit(self):
        """Iterate over a directory of harvested datafiles

        @params:
        path: the directory of content to be transformed and ingested
        """
        #index = Index(self.index)
        for (dirpath, dirnames, filenames) in os.walk(self.folder_in):
            filenames = [ f for f in filenames if f != 'resources.xml' ]
            total = len(filenames)
            c = Counter(total)
            i = 0
            for fname in filenames:
                i += 1
                c.update(i, log)
                document = os.path.join(dirpath, fname)
                log.debug("Processing document: %s" % document)
                #self.process_document(site, document, datatype, stages)

                (identifier, doc) = self.get_identifier(document)
                if identifier is not None:
                    document_name = "%s.xml" % identifier
                    log.debug("Writing out: %s" % os.path.join(self.folder_out, document_name))
                    self.write(doc, os.path.join(self.folder_out, document_name))

    def write(self, element, filename):
        f = open(filename, 'w')
        f.write(etree.tostring(element, pretty_print=True))
        f.close()



