from lxml import etree
import sys
import os
import os.path
import argparse
import logging
from datetime import datetime
from huni.counter import Counter
from huni.clean.date import date_cleanser
from huni.clean.empty import elements

# get the logger
log = logging.getLogger('EXTRACT_LINKS')

class Transform:
    def __init__(self, site, transforms, input_folder, output_folder, debug=False):
        # store the location of the transforms base where we'll go looking
        #  for the required entity transform
        self.transforms = transforms

        # the site we're processing - used in the creation of the document ID
        self.site = site

        # the input folder
        self.input_folder = input_folder

        # the output folder
        self.output_folder = output_folder
        # ensure the output folder exists
        if not os.path.exists(output_folder) and not os.path.isdir(output_folder):
            os.makedirs(output_folder)

        # are we in debug mode?
        self.debug = debug

    def process_path(self):
        """Iterate over a directory of harvested datafiles

        @params:
        path: the directory of content to be transformed and ingested
        """
        #index = Index(self.index)
        for (dirpath, dirnames, filenames) in os.walk(self.input_folder):
            total = len(filenames)
            c = Counter(total)
            i = 0
            for fname in filenames:
                i += 1
                c.update(i, log)
                self.process_document(fname)

    def process_document(self, document):
        """Process a single datafile

        @params:
        document: the file to be processed
        """
        try:
            identity = os.path.basename(document.rstrip('.xml'))
            (site, entity_type, entity_id) = identity.split(':::')
            log.debug("Entity type: %s, entity id: %s" % (entity_type, entity_id))
            transform = "%s/%s.xsl" % (self.transforms, entity_type)
        except IndexError:
            print sys.exc_info()
            log.error("I can't seem to get the entity type from %s, skipping it for now." % document)
            return None

        log.debug("Using %s to transform %s; document id: %s" % (transform, document, identity))
        if not os.path.exists(os.path.join(self.transforms, transform)):
            log.error("Can't find %s so I'm unable to transform %s" % (transform, document))
            return None

        input_document = os.path.join(self.input_folder, document)
        output_document = os.path.join(self.output_folder, document)
        (doc, document) = self.doit(input_document, transform, identity)

        if doc is not None and not self.debug:
            f = open(output_document, 'w')
            f.write(etree.tostring(doc, pretty_print=True, encoding="utf8"))
            f.close()

    def doit(self, document, transform, identity):
        """Transform the document, clean it, submit it

        This method:
         - parses an XML data file,
         - transforms it,

        @params:
        document: the XML file to be transformed
        transform: the XSL transform to use against it
        identity: the value to be used for the document uniqueid
        """
        # read in the XSL transform
        try:
            xslt = etree.parse(transform)
        except etree.XMLSyntaxError, e:
            log.error("Screwy transform; check it! %s" % transform)
            log.error(e)
            sys.exit(1)

        # handle any includes
        xslt.xinclude()

        # read the document to be transformed
        doc = etree.parse(document)

        # transform it
        #  we'll assume the transform will actually load
        #  so we won't bother with exceptions
        transform = etree.XSLT(xslt)
        doc = transform(doc)

        # Check that we got some links. No point writing no links out.
        if len(doc.xpath('/links/link')) == 0:
            return (None, document)

        # get the root of the document
        tmp = doc.xpath('/links')[0]

        log.debug(tmp.items())

        # strip empty elements
        for elem in doc.xpath('/links/link')[0].iter('synopsis'):
            if elem.text is None:
                elem.getparent().remove(elem)

        # add the fromDocId field
        docid = etree.Element('from_docid')
        docid.text = identity.replace(':', '*')
        tmp.insert(0, docid)

        # write to stdout
        log.debug("\n" + etree.tostring(doc, pretty_print=True, encoding="utf8"))

        # return the ready-to-go document
        return (doc, document)


