# Copyright (c) 2013, Deakin University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without 
#  modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice, 
#    this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice, 
#    this list of conditions and the following disclaimer in the documentation 
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
#  POSSIBILITY OF SUCH DAMAGE.

from lxml import etree
import sys
import os
import os.path
import argparse
import logging
from datetime import datetime
from huni.counter import Counter
from huni.clean.date import date_cleanser
from huni.clean.empty import elements 

# get the logger
log = logging.getLogger('TRANSFORM_TO_SOLR')

class Transform:
    def __init__(self, site, transforms, input_folder, output_folder, debug=False):
        # store the location of the transforms base where we'll go looking
        #  for the required entity transform
        self.transforms = transforms
        
        # the site we're processing - used in the creation of the document ID
        self.site = site

        # the input folder
        self.input_folder = input_folder

        # the output folder
        self.output_folder = output_folder
        # ensure the output folder exists
        if not os.path.exists(output_folder) and not os.path.isdir(output_folder):
            os.makedirs(output_folder)

        # the list of date fields in use
        self.date_fields = [
            'startDate', 'endDate',
            'birthDate', 'deathDate',
            'creationDate', 'releaseDate' ]

        # are we in debug mode?
        self.debug = debug

    def process_path(self):
        """Iterate over a directory of harvested datafiles
        
        @params:
        path: the directory of content to be transformed and ingested
        """
        #index = Index(self.index)
        for (dirpath, dirnames, filenames) in os.walk(self.input_folder):
            total = len(filenames)
            c = Counter(total)
            i = 0
            for fname in filenames:
                i += 1
                c.update(i, log)
                self.process_document(fname)

    def process_document(self, document):
        """Process a single datafile

        @params:
        document: the file to be processed
        """
        try: 
            identity = os.path.basename(document.rstrip('.xml'))
            (site, entity_type, entity_id) = identity.split(':::')
            log.debug("Entity type: %s, entity id: %s" % (entity_type, entity_id))
            transform = "%s/%s.xsl" % (self.transforms, entity_type)
        except IndexError:
            print sys.exc_info()
            log.error("I can't seem to get the entity type from %s, skipping it for now." % document)
            return None

        log.debug("Using %s to transform %s; document id: %s" % (transform, document, identity))
        if not os.path.exists(os.path.join(self.transforms, transform)):
            log.error("Can't find %s so I'm unable to transform %s" % (transform, document))
            return None

        input_document = os.path.join(self.input_folder, document)
        output_document = os.path.join(self.output_folder, document)
        (doc, document) = self.doit(input_document, transform, identity)

        if not self.debug:
            f = open(output_document, 'w')
            f.write(etree.tostring(doc, pretty_print=True, encoding="utf8"))
            f.close()

    def clean_dates(self, doc):
        """Date data needs to be in Solr date format

        The format for this date field is of the form 1995-12-31T23:59:59Z,

        @params:
        doc: the XML document
        """
        date_elements = [ e for e in doc.iter() if e.get('name') in self.date_fields ]
        for e in date_elements:
            # have we found an empty or missing date field ?
            if e.text is None:
                continue

            #dc = date_cleanser(self.date_fields)
            dc = date_cleanser()
            datevalue = dc.clean(e.text)
            e.text = datevalue

    def doit(self, document, transform, identity):
        """Transform the document, clean it, submit it

        This method:
         - parses an XML data file, 
         - transforms it,
         - fixes up the known date fields,
         - removes empty elements,
         - submits the document for indexing

        @params:
        document: the XML file to be transformed
        transform: the XSL transform to use against it
        identity: the value to be used for the document uniqueid
        """
        # read in the XSL transform 
        try:
            xslt = etree.parse(transform)
        except etree.XMLSyntaxError, e:
            log.error("Screwy transform; check it! %s" % transform)
            log.error(e)
            sys.exit(1)

        # handle any includes
        xslt.xinclude()

        # read the document to be transformed
        doc = etree.parse(document)

        # transform it
        #  we'll assume the transform will actually load
        #  so we won't bother with exceptions
        transform = etree.XSLT(xslt)
        doc = transform(doc)

        # get the root of the document
        tmp = doc.xpath('/add/doc')[0]

        # add the solr unique id field 
        docid = etree.Element('field', name='docid')
        docid.text = identity.replace(':', '*')
        tmp.append(docid)

        # update the last ingest time (the assumption here is that
        #  the transform happens shortly after harvest)
        doc_lastupdate = etree.Element('field', name="sourceLastUpdate")
        doc_lastupdate.text = datetime.strftime(datetime.now(), "%Y-%m-%dT%H:00:00Z")
        tmp.append(doc_lastupdate)

        # clean the date entries for solr
        self.clean_dates(doc)

        # strip empty elements - dates in particular cause
        #  solr to barf horribly...
        elements().strip_empty_elements(doc)

        # write to stdout
        log.debug("\n" + etree.tostring(doc, pretty_print=True, encoding="utf8"))

        # return the ready-to-go document
        return (doc, document)


