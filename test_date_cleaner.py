import unittest
from huni.clean.date import date_cleanser
import shutil
import os
import os.path
from datetime import date

class Set1(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testDateCleanser(self):
        d = date_cleanser()
        self.assertEqual(d.clean('2013-10-17'), '2013-10-17T00:00:00Z')
        self.assertEqual(d.clean('2013 10 17'), '2013-10-17T00:00:00Z')
        self.assertEqual(d.clean('17 October 2013'), '2013-10-17T00:00:00Z')
        self.assertEqual(d.clean('October 2013'), '2013-10-01T00:00:00Z')
        self.assertEqual(d.clean('234523452345'), None)
