import unittest
from huni.clean.empty import elements
from lxml import etree

class Set1(unittest.TestCase):
    def setUp(self):
        self.doc = """
        <add>
            <doc>
                <field name="1">not empty</field>
                <field name="2"></field>
            </doc>
        </add>
        """

    def tearDown(self):
        pass

    def testEmptyFields(self):
        doc = etree.fromstring(self.doc)
        e = elements()
        e.strip_empty_elements(doc)

        self.assertEqual(len(doc.xpath("//field[@name='1']")), 1)
        self.assertEqual(len(doc.xpath("//field[@name='2']")), 0)
