import unittest
from huni.harvest.harvest import Harvest
import shutil
import os
import os.path
from datetime import date

class Set1(unittest.TestCase):
    def setUp(self):
        self.repo = '/tmp/repo'

#    def tearDown(self):
#        shutil.rmtree(self.repo)

    def testReadConfig(self):
        cfg_file = 'test_data/harvest.cfg'
        h = Harvest(cfg_file, self.repo, False)
        self.assertEqual(h.repository, self.repo)
        self.assertEqual(h.deleted_records, [])
        self.assertEqual(h.datasets['TEST1']['type'], 'OAI')
        self.assertEqual(h.datasets['TEST2']['type'], 'simple')

        cfg_file = 'test_data/bad_file.cfg'
        with self.assertRaises(SystemExit):
            h = Harvest(cfg_file, self.repo, False)

        cfg_file = 'test_data/bad_data_file.cfg'
        with self.assertRaises(SystemExit):
            h = Harvest(cfg_file, self.repo, False)

    def testHarvesting(self):
        cfg_file = 'test_data/harvest.cfg'
        h = Harvest(cfg_file, self.repo, True)

        # test harvesting an OAI site
        h.update('TEST1')
        self.assertTrue(os.path.exists(os.path.join(self.repo, '.resources', str(date.today()), 'TEST1-resources.xml')))
        repo = os.path.join(self.repo, str(date.today()), 'TEST1', 'original')
        self.assertEqual(len(os.listdir(repo)), 9)

        # test harvesting a simple site
        h.update('TEST2')
        self.assertTrue(os.path.exists(os.path.join(self.repo, '.resources', str(date.today()), 'TEST2-resources.xml')))
        repo = os.path.join(self.repo, str(date.today()), 'TEST2', 'original')
        self.assertEqual(len(os.listdir(repo)), 9)

        h.update('TEST3')
        folder = os.path.join(self.repo, str(date.today()), 'OTHER/original')
        self.assertTrue(os.path.exists(folder))
        self.assertEqual(len(os.listdir(folder)), 9)

