import unittest
import shutil
import logging
import os
import os.path
from lxml import etree
logging.basicConfig(level=logging.DEBUG)

class Set1(unittest.TestCase):
    def setUp(self):
        self.repo = '/tmp/repo'

        if not os.path.exists(os.path.join(self.repo, '2013-10-17/FCAC/original')):
            os.makedirs(os.path.join(self.repo, '2013-10-17/FCAC/original'))
        if not os.path.exists(os.path.join(self.repo, '2013-10-17/PDSC/original')):
            os.makedirs(os.path.join(self.repo, '2013-10-17/PDSC/original'))
        if not os.path.exists(os.path.join(self.repo, '2013-10-17/CircusOz/original')):
            os.makedirs(os.path.join(self.repo, '2013-10-17/CircusOz/original'))
        if not os.path.exists(os.path.join(self.repo, '2014-02-26/AustLit/original')):
            os.makedirs(os.path.join(self.repo, '2014-02-26/AustLit/original'))

        shutil.copy('/srv/aggregate/2013-10-17/FCAC/original/AE00001.xml', 
            os.path.join(self.repo, '2013-10-17/PDSC/original'))
        shutil.copy('/srv/aggregate/2013-10-17/FCAC/original/AE00001.xml', 
            os.path.join(self.repo, '2013-10-17/CircusOz/original'))
        shutil.copy('/srv/aggregate/2013-10-17/CircusOz/original/oai-archive.circusoz.com-huni_acts-13493.xml', 
            os.path.join(self.repo, '2013-10-17/FCAC/original'))

    def tearDown(self):
        shutil.rmtree(self.repo)

    def test_eac_preprocess(self):
        shutil.copy('/srv/aggregate/2013-10-17/FCAC/original/AE00001.xml', 
            os.path.join(self.repo, '2013-10-17/FCAC/original'))
        shutil.copy('/srv/aggregate/2013-11-10/FCSA/invalid/SE00001.xml', 
            os.path.join(self.repo, '2013-10-17/FCAC/original'))

        from huni.preprocess.eac import Preprocess
        folder_in = os.path.join(self.repo, '2013-10-17/FCAC/original')
        folder_out = os.path.join(self.repo, '2013-10-17/FCAC/clean')
        site = 'FCAC'
        p = Preprocess(folder_in, folder_out, site)
        p.doit()

        self.assertTrue(os.path.isdir(folder_out))
        self.assertTrue(os.path.isdir(os.path.join(self.repo, '2013-10-17/FCAC/invalid')))
        self.assertTrue(os.path.exists(os.path.join(self.repo, '2013-10-17/FCAC/invalid/SE00001.xml')))

    def test_austlit_preprocess(self):
        shutil.copy('/srv/aggregate/2014-02-26/AustLit/original/6753543.xml', 
            os.path.join(self.repo, '2014-02-26/AustLit/original'))
        shutil.copy('/srv/aggregate/2014-02-26/AustLit/original/6753237.xml', 
            os.path.join(self.repo, '2014-02-26/AustLit/original'))
        shutil.copy('/srv/aggregate/2014-02-26/AustLit/original/6005858.xml', 
            os.path.join(self.repo, '2014-02-26/AustLit/original'))

        from huni.preprocess.austlit import Preprocess
        folder_in = os.path.join(self.repo, '2014-02-26/AustLit/original')
        folder_out = os.path.join(self.repo, '2014-02-26/AustLit/clean')
        site = 'AustLit'
        p = Preprocess(folder_in, folder_out, site)
        p.doit()

        self.assertTrue(os.path.isdir(folder_out))
        self.assertTrue(os.path.exists(os.path.join(self.repo, '2014-02-26/AustLit/clean/')))


    def test_pdsc_preprocess(self):
        shutil.copy('/srv/aggregate/2013-10-17/PDSC/original/oai-paradisec.org.au-AA1', 
            os.path.join(self.repo, '2013-10-17/PDSC/original'))
        shutil.copy('/srv/aggregate/2013-10-17/PDSC/original/oai-paradisec.org.au-AC2-VEMALCV101',
            os.path.join(self.repo, '2013-10-17/PDSC/original'))
        shutil.copyfile('/srv/aggregate/2013-11-10/FCSA/invalid/SE00001.xml', 
            os.path.join(self.repo, '2013-10-17/PDSC/original/oai-paradisec.org.au-SE00001'))

        from huni.preprocess.paradisec import Preprocess
        folder_in = os.path.join(self.repo, '2013-10-17/PDSC/original')
        folder_out = os.path.join(self.repo, '2013-10-17/PDSC/clean')
        site = 'PDSC'
        p = Preprocess(folder_in, folder_out, site)
        p.doit()

        self.assertTrue(os.path.isdir(folder_out))
        self.assertTrue(os.path.isdir(os.path.join(self.repo, '2013-10-17/PDSC/invalid')))
        self.assertTrue(os.path.exists(os.path.join(self.repo, '2013-10-17/PDSC/clean/PDSC:::person:::58.xml')))

    def test_simple_preprocess(self):
        shutil.copy('/srv/aggregate/2013-10-17/CircusOz/original/oai-archive.circusoz.com-huni_acts-13493.xml', 
            os.path.join(self.repo, '2013-10-17/CircusOz/original'))
        shutil.copyfile('/srv/aggregate/2013-11-10/FCSA/invalid/SE00001.xml', 
            os.path.join(self.repo, '2013-10-17/CircusOz/original/oai-archive.circusoz.com-person-SE00001.xml'))

        from huni.preprocess.simple import Preprocess
        folder_in = os.path.join(self.repo, '2013-10-17/CircusOz/original')
        folder_out = os.path.join(self.repo, '2013-10-17/CircusOz/clean')
        site = 'CircusOz'
        p = Preprocess(folder_in, folder_out, site)
        p.doit()

        self.assertTrue(os.path.isdir(folder_out))
        self.assertTrue(os.path.isdir(os.path.join(self.repo, '2013-10-17/CircusOz/invalid')))
        self.assertTrue(os.path.exists(os.path.join(self.repo, '2013-10-17/CircusOz/invalid/oai-archive.circusoz.com-person-SE00001.xml')))

    def test_deleted_files(self):
        if not os.path.exists(os.path.join(self.repo, '2013-10-17/FCNT/original')):
            os.makedirs(os.path.join(self.repo, '2013-10-17/FCNT/original'))
        if not os.path.exists(os.path.join(self.repo, '2013-11-27/FCNT/original')):
            os.makedirs(os.path.join(self.repo, '2013-11-27/FCNT/original'))

        shutil.copy('/srv/aggregate/2013-10-17/FCNT/original/YE00008.xml', 
            os.path.join(self.repo, '2013-10-17/FCNT/original'))

        from huni.preprocess.deleted import Deleted
        doc = etree.Element('deleted')
        record = etree.SubElement(doc, 'record')
        record.text = 'YE00008.xml'

        f = open(os.path.join(self.repo, '2013-11-27/FCNT/original/deleted_records.xml'), 'w')
        f.write(etree.tostring(doc, pretty_print=True))
        f.close()

        folder_in = os.path.join(self.repo, '2013-11-27/FCNT/original')
        deleted_files = os.path.join(folder_in, 'deleted_records.xml')
        d = Deleted(deleted_files)
        d.doit(typeof = 'EAC')



