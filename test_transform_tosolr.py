import unittest
from huni.transform.tosolr import Transform
import shutil
import logging
import os
import os.path
from lxml import etree
logging.basicConfig(level=logging.DEBUG)

class Set1(unittest.TestCase):
    def setUp(self):
        self.repo = '/tmp/repo'
        self.input_folder = os.path.join(self.repo, '2013-10-17/FCAC/clean')
        self.output_folder = os.path.join(self.repo, '2013-10-17/FCAC/solr')
        self.transforms = '/srv/huni-solr-transforms/Solr/FCAC'

        if not os.path.exists(os.path.join(self.repo, '2013-10-17/FCAC/clean')):
            os.makedirs(os.path.join(self.repo, '2013-10-17/FCAC/clean'))

#    def tearDown(self):
#        shutil.rmtree(self.repo)

    def test_transform_to_solr(self):
        shutil.copy('/srv/aggregate/2013-10-17/FCAC/clean/FCAC:::concept:::AE00007.xml', 
            self.input_folder)
        t = Transform('FCAC', self.transforms, self.input_folder, self.output_folder)
        t.process_path()

        #self.assertTrue(os.path.isdir(self.output_folder))
        self.assertTrue(os.path.exists(os.path.join(self.output_folder, 'FCAC:::concept:::AE00007.xml')))

        doc = etree.parse(os.path.join(self.output_folder, 'FCAC:::concept:::AE00007.xml'))
        #print etree.tostring(doc, pretty_print=True)
        self.assertTrue(doc.xpath("//field[@name='type']")[0].text, 'Concept')
        self.assertTrue(doc.xpath("//field[@name='docid']")[0].text, 'FCAC***concept***AE00007')
